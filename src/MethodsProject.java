import java.util.ArrayList;
import java.util.Scanner;

public class MethodsProject {
    public static void main(String[] args) {
        System.out.print("Please enter how many elements you want to add, ");
        int numElements = readInteger();
        System.out.println("Now please enter the values you'd like to add.");
        ArrayList<Integer> elements = readElements(numElements);
        System.out.println("The minimum value is " +findMin(elements));
    }
    //readInteger needs to take user input
    //readElements then needs to take the int from readInteger and store it into an arraylist
    //findMin then needs to take the arraylist from readElements and use it to find the smallest value.
    //or maybe I just write one long code in main that runs all of them with them being each others parameters.

    public static int readInteger() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Integer: ");
        int userNumber = scanner.nextInt();
        return userNumber;
    }

    public static ArrayList<Integer> readElements(int numToRead) {
        ArrayList<Integer> userInputList = new ArrayList<Integer>();
        for (int i = 0; i < numToRead; i++) {
            userInputList.add(readInteger());
        }
        return userInputList;
    }

    public static int findMin(ArrayList<Integer> elements) {
        if (elements.size() == 0) {
            return 0; // Default return if the given list is empty
        }
        int smallest = elements.get(0);
        for (int i = 0; i < elements.size(); i++) {
            if (elements.get(i) < smallest) {
                smallest = elements.get(i);
            }
        }
        return smallest;
    }
}
