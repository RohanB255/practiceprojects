import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;

public class SongLyrics {
    public static void main(String[] args) {
        reversedLyrics("We're no strangers to love");
        reversedLyrics("You know the rules and so do I");
        reversedLyrics("A full commitment's what I'm thinking of");
        reversedLyrics("You wouldn't get this from any other guy");
        System.out.println();
        reversedLyrics("I just wanna tell you how I'm feeling");
        reversedLyrics("Gotta make you understand");
        System.out.println();
        reversedLyrics("Never gonna give you up");
        reversedLyrics("Never gonna let you down");
        reversedLyrics("Never gonna run around and desert you");
        reversedLyrics("Never gonna make you cry");
        reversedLyrics("Never gonna say goodbye");
        reversedLyrics("Never gonna tell a lie and hurt you");
        System.out.println();
        reversedLyrics("We've known each other for so long");
        reversedLyrics("Your heart's been aching, but");
        reversedLyrics("You're too shy to say it");
        reversedLyrics("Inside, we both know what's been going on");
        reversedLyrics("We know the game and we're gonna play it");
        System.out.println();
        reversedLyrics("And if you ask me how I'm feeling");
        reversedLyrics("Don't tell me you're too blind to see");
        System.out.println();
        reversedLyrics("Never gonna give you up");
        reversedLyrics("Never gonna let you down");
        reversedLyrics("Never gonna run around and desert you");
        reversedLyrics("Never gonna make you cry");
        reversedLyrics("Never gonna say goodbye");
        reversedLyrics("Never gonna tell a lie and hurt you");
        System.out.println();
        reversedLyrics("Never gonna give you up");
        reversedLyrics("Never gonna let you down");
        reversedLyrics("Never gonna run around and desert you");
        reversedLyrics("Never gonna make you cry");
        reversedLyrics("Never gonna say goodbye");
        reversedLyrics("Never gonna tell a lie and hurt you");
        System.out.println();
        reversedLyrics("Never gonna give, never gonna give");
        reversedLyrics("Never gonna give, never gonna give");
        System.out.println();
        reversedLyrics("We've known each other for so long");
        reversedLyrics("Your heart's been aching, but");
        reversedLyrics("You're too shy to say it");
        reversedLyrics("Inside, we both know what's been going on");
        reversedLyrics("We know the game and we're gonna play it");
        System.out.println();
        reversedLyrics("I just wanna tell you how I'm feeling");
        reversedLyrics("Gotta make you understand");
        System.out.println();
        reversedLyrics("Never gonna give you up");
        reversedLyrics("Never gonna let you down");
        reversedLyrics("Never gonna run around and desert you");
        reversedLyrics("Never gonna make you cry");
        reversedLyrics("Never gonna say goodbye");
        reversedLyrics("Never gonna tell a lie and hurt you");
        System.out.println();
        reversedLyrics("Never gonna give you up");
        reversedLyrics("Never gonna let you down");
        reversedLyrics("Never gonna run around and desert you");
        reversedLyrics("Never gonna make you cry");
        reversedLyrics("Never gonna say goodbye");
        reversedLyrics("Never gonna tell a lie and hurt you");
        System.out.println();
        reversedLyrics("Never gonna give you up");
        reversedLyrics("Never gonna let you down");
        reversedLyrics("Never gonna run around and desert you");
        reversedLyrics("Never gonna make you cry");
        reversedLyrics("Never gonna say goodbye");
        reversedLyrics("Never gonna tell a lie and hurt you");

    }

    public static ArrayList<String> splitLine(String line) {
        String[] splittedLine = line.split(" ");
        ArrayList<String> lyrics = new ArrayList<String>(Arrays.asList(splittedLine));
//        System.out.println(lyrics);
        return lyrics;
    }

    // lyrics:  [Never, gonna, give, you, up]
    // reverse: [up, you, give, gonna, Never]
    public static void reversedLyrics(String line) { //try to do an array list instead of a string parameter
        ArrayList<String> lyrics = splitLine(line);
        ArrayList<String> reverse = new ArrayList<String>();
        for(int i = lyrics.size() - 1; i >= 0; i--) {
            String toAdd = lyrics.get(i); // you
            reverse.add(toAdd);
        }
        System.out.println(reverse);
    }

//    public static String lyrics() {
//        ArrayList<String> Verse1 = new ArrayList<String>();
//        Verse1.add("We're no strangers to love");
//        Verse1.add("You know the rules and so do I");
//        Verse1.add("A full commitment's what I'm thinking of");
//        Verse1.add("You wouldn't get this from any other guy");
//
//        ArrayList<String> chorus1 = new ArrayList<String>();
//        chorus1.add("I just wanna tell you how I'm feeling");
//        chorus1.add("Gotta make you understand");
//
//        ArrayList<String> chorus = new ArrayList<String>();
//        chorus.add("Never gonna give you up");
//        chorus.add("Never gonna let you down");
//        chorus.add("Never gonna run around and desert you");
//        chorus.add("Never gonna make you cry");
//        chorus.add("Never gonna say goodbye");
//        chorus.add("Never gonna tell a lie and hurt you");
//
//        ArrayList<String> Verse2 = new ArrayList<String>();
//        lyr.add("We've known each other for so long");
//        lyr.add("Your heart's been aching, but");
//        lyr.add("You're too shy to say it");
//        lyr.add("Inside, we both know what's been going on");
//        lyr.add("We know the game and we're gonna play it");

//        ArrayList<String> chorus2 = new ArrayList<String>();
//        chorus2.add("And if you ask me how I'm feeling");
//        chorus2.add("Don't tell me you're too blind to see");
//
//        ArrayList<String> Verse3 = new ArrayList<String>();
//        Verse3.add("Never gonna give, never gonna give");
//        Verse3.add("Never gonna give, never gonna give");
//        return "hello";
    }

