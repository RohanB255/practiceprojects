package InheritanceHW;

public class AdvanceTicket extends Ticket{
    private int daysInAdvance;

    public AdvanceTicket(int ticketNumber, int daysInAdvance) {
        super(ticketNumber);
        this.daysInAdvance = daysInAdvance;
        if(daysInAdvance >= 10) {
            setPrice(30);
        }
        else {
            setPrice(40);
        }

    }
}
