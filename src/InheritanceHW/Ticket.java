package InheritanceHW;

public class Ticket {

    private int ticketNumber;
    private double price;

    public Ticket(int ticketNumber) {
        this.ticketNumber = ticketNumber;
//        this.price = price;
    }

    public void setTicketNumber(int ticketNumber) {
        this.ticketNumber = ticketNumber;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public int getTicketNumber() {
        return ticketNumber;
    }

    public double getPrice() {
        return price;
    }

    @Override
    public String toString() {
        return "Number: " + ticketNumber + ", Price: " + price;
    }
}
