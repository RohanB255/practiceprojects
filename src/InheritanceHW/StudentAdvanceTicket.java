package InheritanceHW;

public class StudentAdvanceTicket extends AdvanceTicket{

    public StudentAdvanceTicket(int ticketNumber, int daysInAdvance) {
        super(ticketNumber, daysInAdvance);
        if(daysInAdvance >= 10) {
            setPrice(15);
        }
        else {
            setPrice(20);
        }
    }

    @Override
    public String toString() {
        return super.toString() + " (ID Required)";
    }


}
