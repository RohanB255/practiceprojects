package FinancesProject;

public class FinanceConstructionException extends Exception {

    public FinanceConstructionException(String str) {
        super(str);
    }
}
