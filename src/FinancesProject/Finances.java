package FinancesProject;

public class Finances {
    //written question 1: Instance methods can access both static and instance fields because they need to first
    //create an object, which works with both static and instances. Static methods however only work with static fields
    //because they can't have an object created and can only access a reference.
    //written question 2: An example of a common class that contains mainly static methods would be a temperature
    //converter because there is no object being created and instead data is just being changed.

    private static final double permanentInterestRate = 0.1;
    public static void main(String[] args) {
        System.out.println(calculateSimpleInterest(100));
        System.out.println(calculateSimpleInterest(-1));
        System.out.println(calculateMonthsUntilPaidOff(100, 10));
        System.out.println(calculateMonthsUntilPaidOff(100, 0));

    }

    public Finances() throws FinanceConstructionException {
        throw new FinanceConstructionException("");
    }

    public static double calculateSimpleInterest(double amountOfMoneyInterestIsOwedOn) {
        double interestOwed = 0;

        try {
            if(amountOfMoneyInterestIsOwedOn < 0) {
                throw new IllegalArgumentException();
            }
            interestOwed = amountOfMoneyInterestIsOwedOn * permanentInterestRate;
        } catch (IllegalArgumentException e) {
            System.out.println("You can't owe a negative amount of money.");
        }
        return interestOwed;
    }

    public static double calculateMonthsUntilPaidOff(double amountOwed, double monthlyPaymentAmount) {
        int monthsToPayOffItem = 0;
        try {
            if(amountOwed < 0 || monthlyPaymentAmount <= 0) {
                throw new IllegalArgumentException();
            }
            monthsToPayOffItem = (int) (amountOwed / monthlyPaymentAmount) + 1;
        }catch (IllegalArgumentException e) {
            System.out.println("You cant owe less than 0 dollars and you can't pay 0 or less dollars");
        }
        return monthsToPayOffItem;
    }
}
