package CourseSchedulingTest;

import java.sql.Time;


public class TimeSlot {

    public enum Weekday{
        MONDAY,
        TUESDAY,
        WEDNESDAY,
        THURSDAY,
        FRIDAY
    }

    private Weekday dayOfClass;
    private int startingTime;
    private int durationOfCourse;

    public TimeSlot(Weekday dayOfClass, int startingTime, int durationOfCourse) {
        if(startingTime < 7 || startingTime > 17) {
            throw new IllegalArgumentException("Classes can only be from 7:00 to 17:00");
        }
        this.dayOfClass = dayOfClass;
        this.startingTime = startingTime;
        this.durationOfCourse = durationOfCourse;
    }

    public Weekday getDayOfClass() {
        return dayOfClass;
    }

    public int getStartingTime() {
        return startingTime;
    }

    public int getDurationOfCourse() {
        return durationOfCourse;
    }

    public void setDayOfClass(Weekday dayOfClass) {
        this.dayOfClass = dayOfClass;
    }

    public void setStartingTime(int startingTime) {
        this.startingTime = startingTime;
    }

    public void setDurationOfCourse(int durationOfCourse) {
        this.durationOfCourse = durationOfCourse;
    }

    public boolean conflictsWith(TimeSlot slot) {
        // slot.startingTime, slotEndingTime = slot.startingTime + slot.duration
        // this.startingTIme, thisEndingTIme = this.startingTIme + this.duration
        //
        // this.startingTime <= slot.startingTime < thisEndingTime (invalid)
        // this.starginTIme <+ slot.endingTIme <= thisEndingTime
        if (slot.dayOfClass == dayOfClass) {
            if (this.startingTime <= slot.startingTime && slot.startingTime < this.startingTime + durationOfCourse) {
                return true;
            } else if (this.startingTime < slot.startingTime + slot.durationOfCourse && slot.startingTime +
                    slot.durationOfCourse <= this.startingTime + slot.durationOfCourse) {
                return true;
            }
        }
        return false;
    }

    @Override
    public String toString() {
        return "Time: " + startingTime + "-" + (startingTime + durationOfCourse) + ", Day: " + dayOfClass;
    }


}
