package CourseSchedulingTest;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) throws FileNotFoundException {
        ArrayList<Course> allCourses = CourseLoader.loadCourses();
        for(int i = 0; i < allCourses.size(); i++) {
            System.out.println(i + 1 + ". " + allCourses.get(i));
        }

        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter the code for your 6 requested courses in order of most important to least important: ");
        ArrayList<Course> requestedCourses = generateCourses(scanner.nextLine(), allCourses);
        System.out.println("Enter the code for your 2 alternate courses in order of most important to least important: ");
        ArrayList<Course> alternateCourses = generateCourses(scanner.nextLine(), allCourses);

        ArrayList<Course> schedule = createSchedule(requestedCourses, alternateCourses);
        System.out.println("Your schedule is: ");
        int totalHWHours = 0;
        for(int i = 0; i < schedule.size(); i++) {
            totalHWHours = schedule.get(i).getWeeklyHomeworkHours() + totalHWHours;
            System.out.println(i + 1 + ". " + schedule.get(i) + ", " + schedule.get(i).getTimeOfClass());
        }
        System.out.println("Your total hours of homework per week: " + totalHWHours);
    }

    public static ArrayList<Course> createSchedule(ArrayList<Course> requestedCourses, ArrayList<Course> alternateCourses) {
        ArrayList<Course> finalSchedule = new ArrayList<>();
        for(Course course : requestedCourses) {
            if(canAddToSchedule(finalSchedule, course)) {
                finalSchedule.add(course);
            }
            else if(canAddToSchedule(finalSchedule, alternateCourses.get(0))) {
                finalSchedule.add(alternateCourses.get(0));
            }
            else {
                finalSchedule.add(alternateCourses.get(1));
            }
        }
        return finalSchedule;
    }

    public static ArrayList<Course> generateCourses(String coursesWanted, ArrayList<Course> allCourses) {
        String[] arrayCoursesWanted = coursesWanted.split("\\s+");

        ArrayList<Course> courses = new ArrayList<>();
        for(String courseCode : arrayCoursesWanted) {
            for(Course code : allCourses) {
                if(courseCode.equals(code.getCourseCode())) {
                    courses.add(code);
                    break;
                }
            }
        }
        return courses;
    }

    public static boolean canAddToSchedule(ArrayList<Course> schedule, Course courseToAdd) {
        for(Course course : schedule) {
            if(courseToAdd.getTimeOfClass().conflictsWith(course.getTimeOfClass())) {
                return false;
            }
        }
        return true;
    }


}
