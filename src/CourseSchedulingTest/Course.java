package CourseSchedulingTest;

public class Course {
    private String name;
    private String courseCode;
    private int weeklyHomeworkHours;
    private TimeSlot timeOfClass;

    public Course(String name, String courseCode, int weeklyHomeworkHours, TimeSlot timeOfClass) {
        this.name = name;
        this.courseCode = courseCode;
        this.weeklyHomeworkHours = weeklyHomeworkHours;
        this. timeOfClass = timeOfClass;
    }

    public String getName() {
        return name;
    }

    public String getCourseCode() {
        return courseCode;
    }

    public int getWeeklyHomeworkHours() {
        return weeklyHomeworkHours;
    }

    public TimeSlot getTimeOfClass() {
        return timeOfClass;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setCourseCode(String courseCode) {
        this.courseCode = courseCode;
    }

    public void setWeeklyHomeworkHours(int weeklyHomeworkHours) {
        this.weeklyHomeworkHours = weeklyHomeworkHours;
    }

    public void setTimeOfClass(TimeSlot timeOfClass) {
        this.timeOfClass = timeOfClass;
    }

    @Override
    public String toString() {
        return "Course: " + name + ", " + courseCode + ", HW Hours: " + weeklyHomeworkHours;
    }
}
