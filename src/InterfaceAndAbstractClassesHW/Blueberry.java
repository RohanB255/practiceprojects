package InterfaceAndAbstractClassesHW;

public class Blueberry extends Crop implements ProcessableIF {

    public Blueberry() {
        super("Blueberry", 1.99, 3);
    }

    public void process() {
        setName("blueberry jam");
        setBaseSellingPrice(getBaseSellingPrice()*2);
    }

}
