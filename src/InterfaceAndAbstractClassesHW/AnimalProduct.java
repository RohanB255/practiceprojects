package InterfaceAndAbstractClassesHW;

public class AnimalProduct extends Product {

    public AnimalProduct(String name, double baseSellPrice) {
        super(name,baseSellPrice);
    }

    @Override
    public double calculateSellPrice() {
        double sellPrice = super.getBaseSellingPrice() * super.getQuality();
        return sellPrice;
    }
}
