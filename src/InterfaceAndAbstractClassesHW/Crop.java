package InterfaceAndAbstractClassesHW;

public class Crop extends  Product{
    private int cropsPerHarvest;

    public Crop(String name, double baseSellingPrice, int cropsPerHarvest) {
        super(name, baseSellingPrice);
        this.cropsPerHarvest = cropsPerHarvest;
    }

    public int getCropsPerHarvest() {
        return cropsPerHarvest;
    }

    @Override
    public double calculateSellPrice() {
        double sellPrice = super.getBaseSellingPrice() * super.getQuality() * getCropsPerHarvest();
        return sellPrice;
    }
}
