package InterfaceAndAbstractClassesHW;

public class Milk extends Crop implements ProcessableIF{

    public Milk() {
        super("milk", 1.49, 7);
    }

    public void process() {
        setName("cheese");
        setBaseSellingPrice(getBaseSellingPrice() * 2);
    }
}
