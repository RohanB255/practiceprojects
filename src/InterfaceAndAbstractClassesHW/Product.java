package InterfaceAndAbstractClassesHW;

import java.util.Random;

public abstract class Product {
    private String name;
    private int quality;
    private boolean isHarvested;
    private double baseSellingPrice;

    public Product(String name, double baseSellingPrice) {
        this.name = name;
        this.quality = 1;
        this.isHarvested = false;
        this.baseSellingPrice = baseSellingPrice;
    }

    public String getName() {
        return name;
    }

    public int getQuality() {
        return quality;
    }

    public boolean getIsHarvested() {
        return isHarvested;
    }

    public double getBaseSellingPrice() {
        return baseSellingPrice;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setBaseSellingPrice(double baseSellingPrice) {
        this.baseSellingPrice = baseSellingPrice;
    }

    public void setHarvested(boolean harvested) {
        isHarvested = harvested;
    }

    public void setQuality(int quality) {
        this.quality = quality;
    }

    public void harvest(Product product) {
        if(!product.getIsHarvested()) {
            Random rand = new Random();
            int quality = rand.nextInt(4) + 1;
            product.setHarvested(true);
            product.setQuality(quality);
        }
    }

    abstract double calculateSellPrice();
}
