package PlaylistManager;

public class Song {
    private String name;
    private int duration;
    private static final String artist = "Kendrick Lamar";

    public Song(String name, int duration, String artist) {
        this.name = name;
        positiveInt(duration);
//        this.artist = artist;
    }

    public String getName() {
        return name;
    }

    public int getDuration() {
        return duration;
    }

    public static String getArtist() {
        return artist;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setDuration(int duration) {
        positiveInt(duration);
    }

//    public void setArtist(String artist) {
//        this.artist = artist;
//    }

    public void positiveInt(int number) {
        if(number > 0) {
            this.duration = number;
        }
        else {
            this.duration = 0;
        }
    }

    @Override
    public String toString() {
        return name + " by " + artist + "; length: " + duration + " seconds";
    }

}
