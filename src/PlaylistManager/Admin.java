package PlaylistManager;

public class Admin extends User {

    private static final Admin INSTANCE = new Admin("instance");

    private Admin(String username) {
        super(username);
    }

    public void changeUsername(User userBeingChanged, String newUsername) {
        String username = userBeingChanged.getUsername();
        userBeingChanged.setUsername(newUsername);
    }

    public static Admin getInstance() {
        return INSTANCE;
    }
}
