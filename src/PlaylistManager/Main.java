package PlaylistManager;

import java.sql.SQLOutput;

public class Main {
    public static void main(String[] args) {
        Song song1 = new Song("Humble", 180, "Kendrick Lamar");
        Playlist playlist1 = new Playlist("Rap");
        playlist1.addSong(song1);
        playlist1.printSongs();
        Song song2 = new Song("DNA", 240, "Kendrick Lamar");
        playlist1.addSong(song2);
        playlist1.printSongs();
        Song song3 = new Song("Good For You", 180, "Olivia Rodrigo");
        Playlist playlist2 = new Playlist("Pop");
        playlist2.addSong(song3);
        playlist2.printSongs();
        Song song4 = new Song("Levitating", 240, "Dua Lipa and DaBaby");
        playlist2.addSong(song4);
        playlist2.printSongs();
        User user1 = new User("Rohan");
        user1.addPlaylist(playlist1);
        user1.printPlaylists();
        user1.addPlaylist(playlist2);
        user1.printPlaylists();
        Song song5 = new Song("Middle child", 240, "J cole");
        user1.addSongToPlaylist(user1, "Rap", song5);
        playlist1.printSongs();
        Admin.getInstance().changeUsername(user1, "frank");
        System.out.println(user1.getUsername());
    }
}
