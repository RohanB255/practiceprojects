package PlaylistManager;

import java.util.ArrayList;

public class User {
    private String username;
    private ArrayList<Playlist> listOfPlaylists;

    public User(String name) {
        ArrayList<Playlist> emptyArrayList = new ArrayList<>();
        listOfPlaylists = emptyArrayList;
        this.username = name;
    }

    public void addPlaylist(Playlist playlistToAdd) {
        listOfPlaylists.add(playlistToAdd);
    }

    public void printPlaylists() {
        ArrayList<Playlist> playlistsPrinting = listOfPlaylists;
        System.out.println(playlistsPrinting);
    }

    public ArrayList<Playlist> getListOfPlaylists() {
        return listOfPlaylists;
    }

    public boolean addSongToPlaylist(User user, String nameOfPlaylist, Song songToAdd) {
        ArrayList<Playlist> playlists = user.getListOfPlaylists();
        for (Playlist playlistBeingChecked : playlists) {
            String nameOfPlaylistBeingChecked = playlistBeingChecked.getName();
            if (nameOfPlaylist.equalsIgnoreCase(nameOfPlaylistBeingChecked)) {
                playlistBeingChecked.addSong(songToAdd);
                return true;
            }
        }
        return false;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username){
        this.username = username;
    }
}

