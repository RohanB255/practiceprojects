package PlaylistManager;

import java.lang.reflect.Array;
import java.util.ArrayList;

public class Playlist {
    private String name; //name of playlist
    private int duration; //how long the playlist is
    private ArrayList<Song> listOfSongs; //the songs in the playlist

    public Playlist(String name) {
        ArrayList<Song> emptyArrayList = new ArrayList<>();
        listOfSongs = emptyArrayList;
        this.name = name;
        duration = 0;
    }

    public void addSong(Song songToAdd) {
        listOfSongs.add(songToAdd);
        int durationToAdd = songToAdd.getDuration();
        duration = duration + durationToAdd;
    }

    public void printSongs() {
        ArrayList<Song> songsToPrint = listOfSongs;
        System.out.println(songsToPrint);
    }

    public ArrayList<Song> getListOfSongs() {
        return listOfSongs;
    }

    public int getDuration() {
        return duration;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return name + "; " + duration + " seconds";
    }
}
