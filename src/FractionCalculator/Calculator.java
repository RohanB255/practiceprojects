package FractionCalculator;

import java.sql.SQLOutput;
import java.util.Arrays;
import java.util.Scanner;

public class Calculator {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        Fraction num1 = fractionSplit("First", scanner);
        Fraction num2 = fractionSplit("Second", scanner);
        System.out.println("Operation: ");
        String operation = scanner.nextLine();
        switch (operation) {
            case "+":
                System.out.println("The answer is: " + addition(num1, num2));
                break;
            case "-":
                System.out.println("The answer is: " + subtraction(num1, num2));
                break;
            case "*":
                System.out.println("The answer is: " + multiplication(num1, num2));
                break;
            case "/":
                System.out.println("The answer is: " + division(num1, num2));
                break;
        }
    }

    public static Fraction addition(Fraction num1, Fraction num2) {
        int wholeNumber1 = num1.getWholeNumber();
        int wholeNumber2 = num2.getWholeNumber();
        int numerator1 = (num1.getNumerator() * num2.getDenominator());
        int numerator2 = (num2.getNumerator() * num1.getDenominator());
        int denominator = (num1.getDenominator() * num2.getDenominator());
        Fraction sum= new Fraction(wholeNumber1 + wholeNumber2, numerator1 + numerator2,
                denominator);
        Fraction simplifiedSum = simplification(sum);
        if(simplifiedSum.getNumerator() > simplifiedSum.getDenominator()) {
            int convertedWholeNumber = simplifiedSum.getNumerator() / simplifiedSum.getDenominator();
            int convertedNumerator = simplifiedSum.getNumerator() % simplifiedSum.getDenominator();
            simplifiedSum.setWholeNumber(convertedWholeNumber);
            simplifiedSum.setNumerator(convertedNumerator);
        }
        return simplifiedSum;
    }
//
    public static Fraction subtraction(Fraction num1, Fraction num2) {
        int numerator1 = ((num1.getWholeNumber() * num1.getDenominator()) + num1.getNumerator());
        int numerator2 = ((num2.getWholeNumber() * num2.getDenominator()) + num2.getNumerator());
        numerator1 = (numerator1 * num2.getDenominator());
        numerator2 = (numerator2 * num1.getDenominator());
        int denominator = (num1.getDenominator() * num2.getDenominator());
        Fraction difference = new Fraction(0, numerator1 - numerator2,
                denominator);
      Fraction simplifiedDifference = simplification(difference);
      if(simplifiedDifference.getNumerator() > simplifiedDifference.getDenominator()) {
          int convertedWholeNumber = simplifiedDifference.getNumerator() / simplifiedDifference.getDenominator();
          int convertedNumerator = simplifiedDifference.getNumerator() % simplifiedDifference.getDenominator();
          simplifiedDifference.setWholeNumber(convertedWholeNumber);
          simplifiedDifference.setNumerator(convertedNumerator);
      }
        return simplifiedDifference;
    }

    public static Fraction multiplication(Fraction num1, Fraction num2) {
        int numerator1 = ((num1.getWholeNumber() * num1.getDenominator()) + num1.getNumerator());
        int numerator2 = ((num2.getWholeNumber() * num2.getDenominator()) + num2.getNumerator());
        Fraction product = new Fraction(0, numerator1 * numerator2,num1.getDenominator() * num2.getDenominator());
        Fraction simplifiedProduct = simplification(product);
        if(simplifiedProduct.getNumerator() > simplifiedProduct.getDenominator()) {
          int convertedWholeNumber = simplifiedProduct.getNumerator() / simplifiedProduct.getDenominator();
          int convertedNumerator = simplifiedProduct.getNumerator() % simplifiedProduct.getDenominator();
          simplifiedProduct.setWholeNumber(convertedWholeNumber);
          simplifiedProduct.setNumerator(convertedNumerator);
        }
        return simplifiedProduct;
    }

    public static Fraction division(Fraction num1, Fraction num2) {
        int numerator2 = ((num2.getWholeNumber() * num2.getDenominator()) + num2.getNumerator());
        num2.setNumerator(num2.getDenominator());
        num2.setDenominator(numerator2);
        num2.setWholeNumber(0);
        Fraction quotient = multiplication(num1, num2);
        return quotient;
    }

    public static Fraction fractionSplit( String number, Scanner scanner) {
        System.out.println( number + " fraction, put the fraction as whole number numerator / denominator (if it's " +
                "not a mixed number put 0 for the whole number): ");
        String fractionValues = scanner.nextLine();
        String[] arrayOfFraction = fractionValues.split("\\s+");
        int fractionWholeNumber = Integer.parseInt(arrayOfFraction[0]);
        int fractionNumerator = Integer.parseInt(arrayOfFraction[1]);
        int fractionDenominator = Integer.parseInt(arrayOfFraction[3]);
        while(fractionDenominator == 0) {
            System.out.println("The denominator can't be 0");
            System.out.println( number + " fraction, put the fraction as whole number numerator / denominator " +
                    "(if it's not a mixed number put 0 for the whole number): ");
            fractionValues = scanner.nextLine();
            arrayOfFraction = fractionValues.split("\\s+");
            fractionWholeNumber = Integer.parseInt(arrayOfFraction[0]);
            fractionNumerator = Integer.parseInt(arrayOfFraction[1]);
            fractionDenominator = Integer.parseInt(arrayOfFraction[3]);
        }
        Fraction fraction = new Fraction(fractionWholeNumber, fractionNumerator, fractionDenominator);
        simplification(fraction);
        return fraction;
    }

    public static Fraction simplification(Fraction fraction) {
        int numerator = fraction.getNumerator();
        int denominator = fraction.getDenominator();
        int wholeNumber= fraction.getWholeNumber();
        if(Math.abs(numerator) > Math.abs(denominator)) {
            int i = denominator;
            while (i >= 2) {
                int remainder = denominator % i;
                if (remainder == 0) {
                    int remainder2 = numerator % i;
                    if (remainder2 == 0) {
                        denominator = denominator / i;
                        numerator = numerator / i;
                    }
                }
                i--;
            }
        }
        else if(Math.abs(denominator) > Math.abs(numerator)) {
            int i = numerator - 1; {
                while (i >= 2) {
                    int remainder = numerator % i;
                    if(remainder == 0) {
                        int remainder2 = denominator % i;
                        if(remainder2 == 0) {
                            numerator = numerator / i;
                            denominator = denominator / i;
                        }
                    }
                    i--;
                }
            }
        }
        else {
            wholeNumber = wholeNumber + 1;
            numerator = 0;
            denominator = 0;
        }
        Fraction simplifiedFraction = new Fraction(wholeNumber, numerator, denominator);
        return simplifiedFraction;
    }
}

