package FractionCalculator;

public class Fraction {
    private int numerator;
    private int denominator;
    private int wholeNumber;


    public Fraction(int wholeNumber, int numerator, int denominator) {
        this.numerator = numerator;
        this.denominator = denominator;
        this.wholeNumber = wholeNumber;
    }
    public int getNumerator() {
        return numerator;
    }
    public int getDenominator() {
        return denominator;
    }
    public void setNumerator(int numerator) {
        this.numerator = numerator;
    }
    public void setDenominator(int denominator) {
        this.denominator = denominator;
    }
    public int getWholeNumber() {
        return wholeNumber;
    }
    public void setWholeNumber(int wholeNumber) {
        this.wholeNumber = wholeNumber;
    }

    @Override
    public String toString() {
        if(numerator > 0 && denominator > 0) {
            return wholeNumber + " " + numerator + "/" + denominator;
        }
        else {
            return wholeNumber + "";
        }

    }
}
