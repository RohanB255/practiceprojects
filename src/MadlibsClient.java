import java.sql.SQLOutput;
//import java.util.Scanner;

public class MadlibsClient {

    public static void main(String[] args) {
//        // User input code:
//        //Scanner scanner = new Scanner(System.in);
//       // System.out.println("Adjective: ");
//       // String adj = scanner.nextLine();
//        //System.out.println("Noun: ");
//        //String noun1 = scanner.nextLine();
//        //System.out.println("Plural noun");
//        //String pluralNoun = scanner.nextLine();
//        //System.out.println("Noun");
//        //String noun2 = scanner.nextLine();
//        //System.out.println("Adjective");
//        //String adjective = scanner.nextLine();
//
//        //Final Result
//        //System.out.println("There are many " + adj + " ways to choose a " + noun1 + " to read. First, you could ask");
//        //System.out.println("for recommendations from your friends and " + pluralNoun + ". Just don't ask the ");
//        //System.out.println(noun2 + ", it only reads " + adjective + " books.");
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//        //If statements
//      //  if (4 > 2) {
//     //       System.out.println("4 is greater than 2.");
//        }
//
//        //Comparison operators
//     //   int num1 = 7;
//     //   int num2 = 10;
//     //   if(num1 == num2) {
//            //code
//        }
//      //  if (num1 <= num2) {
//            //code
//        }
//        //multiple conditions
//        //&& (and) ||(or)
//      //  if(num1 == 7 && num2 <15){
//            //code
//      //  }
//    //    if num1 == 7 || num2 <15) {
//    //code
//      //  }
//
//        //not operator (!)
//     //   boolean bool = true;
//    //    if (!bool) {
//         //   System.out.println("This won't run");
//     //   }
//     //   if(num1 != num2) {
//     //       System.out.println("They're different");
//     //   }
//        //Comparing Strings
//     //   String str = "bob";
//     //   if(str.equals("bob")) {
//            //code
//      //  }
//
//        //Else if and else
//      //  int num = 4;
//      //  if (num < 0) {
//     //       System.out.println("negative");
//     //   } else if (num >0 && num is < 10) {  //can have multiple
//     //       System.out.println("num is between 0 and 10");
//     //   } else {
//     //       System.out.println("We got to the else statement");
//      //  }

//        // Check for understanding
//                int age = 14;
//                if (age < 10 ){
//        System.out.println("you're too young");
//        }else if (age >10 && age < 13) {
//                    System.out.println("you have child permissions");
//        } else{
//                    System.out.println("you have full access");
//        }
//
//        //The switch statement
//        String month = "jan";
//
//        switch(month) {
//            case "dec":
//            case "jan":
//                //both lead to this
//                System.out.println("it's cold");
//                System.out.println("And it's January");
//                break;
//            case "feb":
//                System.out.println("Happy valentine's day");
//                break;
//            //Can do as many as you want
//            default: //this happens if no cases are true
//                System.out.println("This is the default");
//                break;
        String name = "fred";
        switch(name) {
            case "bob":
                System.out.println("I have an uncle named Bob");
                break;
            case "fred":
                System.out.println("Hey, my name's fred too!");
                break;
            default:
                System.out.println("Hmm, I don't know anyone named " + name);
                break;


        }


    }



}
