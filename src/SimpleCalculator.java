import java.util.Scanner;


public class SimpleCalculator {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("First Number: ");
        double number1 = scanner.nextDouble();
        scanner.nextLine();
        System.out.println("Operation: ");
        String operation = scanner.nextLine();
        System.out.println("Second Number: ");
        double number2 = scanner.nextDouble();
        scanner.nextLine();
        switch(operation) {
            case "+":
                double answer = number1 += number2;
                System.out.println("The answer is: " + answer);
                break;
            case "-":
                double answer1 = number1 -= number2;
                System.out.println("The answer is: " + answer1);
                break;
            case "*":
                double answer2 = number1 *= number2;
                System.out.println("The answer is: " + answer2);
                break;
            case "/":
                if (number2 == 0) {
                    System.out.println("Error");
                    break;
                }
                double answer3 = number1 /= number2;
                System.out.println("The answer is: " + answer3);
                break;
            default:
                System.out.println("Error");
                break;
        }



    }
}
