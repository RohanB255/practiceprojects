import java.util.Scanner;

public class ForAndWhileLoops {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Project: ");
        String project = scanner.nextLine();
        System.out.println("given project: " + project);
        if (project.equals("while")) {
            int input = 0;
            int num = (int) (Math.random() * 100);
            while (input != num) {
                System.out.println("Integer: ");
                input = scanner.nextInt();
                if (input < num) {
                    System.out.println("You are lower than the number");
                }
                if (input > num) {
                    System.out.println("You are higher than the number");
                }
            }
        }
        else {
            System.out.println("Age: ");
            int age = scanner.nextInt();
            for (int i = 2 - (age % 2); i != age; i += 2) {
                System.out.println(i);
            }
        }
    }

}




